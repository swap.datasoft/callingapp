package com.example.calling;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {


    public static final String MY_PREFS_NAME = "SCHEDULE_CALL_TIMER";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    private static final String TIME = "TIME";
    private Button sampleButton;
    private TextView sampleTextView;
    private Context mContext;
    private EditText editTextNumber, editTextTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = getApplicationContext();

        sampleButton = findViewById(R.id.btSampleButton);
        editTextNumber = findViewById(R.id.edit_text_phone_number);
        editTextTime = findViewById(R.id.edit_text_time);

        sampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (editTextNumber.getText().toString().trim().isEmpty()) {
                    Toast.makeText(mContext, "Please input your number", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (editTextTime.getText().toString().trim().isEmpty()) {
                    Toast.makeText(mContext, "Please input your time", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (ActivityCompat.checkSelfPermission(mContext,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                    return;
                }

                saveDataAndStartService();

            }
        });


    }

    private void saveDataAndStartService() {

        saveData(editTextNumber.getText().toString().trim(), editTextTime.getText().toString().trim());

        int hour = 0, minute = 0;

        if (editTextTime.getText().toString().contains(":")) {
            hour = Integer.parseInt(editTextTime.getText().toString().trim().split(":")[0]);
            minute = Integer.parseInt(editTextTime.getText().toString().trim().split(":")[1]);
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

        Intent intent = new Intent(MainActivity.this, MyService.class);
        PendingIntent pintent = PendingIntent.getService(MainActivity.this, 0, intent, 0);
        AlarmManager alarm_manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (alarm_manager != null)
            alarm_manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pintent);

    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT).show();
                    saveDataAndStartService();
                } else {
                    Toast.makeText(mContext, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

// other 'case' lines to check for other
// permissions this app might request
        }
    }


    private void saveData(String phone_number, String time) {
        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(PHONE_NUMBER, phone_number);
        editor.putString(TIME, time);
        editor.apply();
    }

}
