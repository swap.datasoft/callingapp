package com.example.calling;


import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.calling.MainActivity.MY_PREFS_NAME;
import static com.example.calling.MainActivity.PHONE_NUMBER;


public class MyService extends Service {

    public static final int notify = 20000;  //interval between two services(Here Service run every 5 seconds)
    int count = 0;  //number of times service is display
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;    //timer handling
    private Context mContext;
    private String phoneNumbers = "";
    private String[] phoneNumbersArray = new String[10];

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {

        mContext = getApplicationContext();

        Toast.makeText(mContext, "service on create", Toast.LENGTH_SHORT).show();

        phoneNumbers = getPhoneNumber();
        if (phoneNumbers.contains(",")) {
            phoneNumbersArray = getPhoneNumber().split(",");
        } else {
            phoneNumbersArray[0] = phoneNumbers;
        }

        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else
            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();    //For Cancel Timer
        Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();
    }

    //class TimeDisplay for handling task
    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    // display toast



                    if (ActivityCompat.checkSelfPermission(mContext,
                            Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                        if (count == phoneNumbersArray.length)
                            stopSelf();

                        if (phoneNumbersArray[count] != null) {
                            if (phoneNumbersArray[count].isEmpty())
                                return;
//                            endCall();
                            final Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            callIntent.setData(Uri.parse("tel:" + phoneNumbersArray[count]));
                            startActivity(callIntent);
                            count += 1;
                        }
                    }

                    //Toast.makeText(MyService.this, "Service is running", Toast.LENGTH_SHORT).show();
                }
            });

        }

    }

    private void endCall() {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        Class clazz = null;
        try {
            clazz = Class.forName(telephonyManager.getClass().getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Method method = null;
        try {
            method = clazz.getDeclaredMethod("getITelephony");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        method.setAccessible(true);
        ITelephony telephonyService = null;
        try {
            telephonyService = (ITelephony) method.invoke(telephonyManager);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        try {
            telephonyService.endCall();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getPhoneNumber() {

        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString(PHONE_NUMBER, null);
        return restoredText;

    }
}

